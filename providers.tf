terraform {
  required_version = ">= 1.0"
  backend "s3" {
    bucket = "infra-isreal-state"
    key    = "infra/state.tfstate"
    region = "us-east-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.20"
    }
  }
}